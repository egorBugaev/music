import React from 'react';
import {Image, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';


import notFound from '../../assets/images/not-found.png';

const AlbumListItem = props => {
	let image = notFound;
	if (props.image) {
		image = "data:image/auto;base64," + props.image;
	}

	return (
		<Panel>
			<Panel.Body>
				<Image
					style={{width: '100px', marginRight: '10px'}}
					src={image}
					thumbnail
				/>
				<Link to={'/tracks/' + props.id}>
					{props.title}
				</Link>
			</Panel.Body>
		</Panel>
	);
};

AlbumListItem.propTypes = {
	id: PropTypes.string.isRequired,
	title: PropTypes.string.isRequired,
	image: PropTypes.string
};

export default AlbumListItem;