import React, {Component} from 'react';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";


class AlbumForm extends Component {

	state = {
		artist: '',
		title: '',
		description: '',
		year:'',
		image: ''
	};
	componentDidMount() {
		console.log(this.props);
		this.setState({artist: this.props.artistId});
	}

	submitFormHandler = event => {
		event.preventDefault();

		const formData = new FormData();
		Object.keys(this.state).forEach(key => {
			formData.append(key, this.state[key]);
		});

		this.props.onSubmit(formData);
	};

	inputChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.value
		});
	};

	fileChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.files[0]
		});
	};

	render() {
		return (
			<Form horizontal onSubmit={this.submitFormHandler}>
				<FormGroup controlId="albumTitle">
					<Col componentClass={ControlLabel} sm={2}>
						Title
					</Col>
					<Col sm={10}>
						<FormControl
							type="text" required
							placeholder="Enter artist name"
							name="title"
							value={this.state.title}
							onChange={this.inputChangeHandler}
						/>
					</Col>
				</FormGroup>

				<FormGroup controlId="albumDescription">
					<Col componentClass={ControlLabel} sm={2}>
						Description
					</Col>
					<Col sm={10}>
						<FormControl
							componentClass="textarea"
							placeholder="Enter description"
							name="description"
							value={this.state.description}
							onChange={this.inputChangeHandler}
						/>
					</Col>
				</FormGroup>
				<FormGroup controlId="albumYear">
					<Col componentClass={ControlLabel} sm={2}>
						Year
					</Col>
					<Col sm={10}>
						<FormControl
							componentClass="textarea"
							placeholder="Enter year"
							name="year"
							value={this.state.year}
							onChange={this.inputChangeHandler}
						/>
					</Col>
				</FormGroup>

				<FormGroup controlId="productImage">
					<Col componentClass={ControlLabel} sm={2}>
						Image
					</Col>
					<Col sm={10}>
						<FormControl
							type="file"
							name="image"
							onChange={this.fileChangeHandler}
						/>
					</Col>
				</FormGroup>

				<FormGroup>
					<Col smOffset={2} sm={10}>
						<Button bsStyle="primary" type="submit">Save</Button>
					</Col>
				</FormGroup>
			</Form>
		);
	}
}

export default AlbumForm;
