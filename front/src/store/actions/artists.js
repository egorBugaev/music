import axios from '../../axios-api';
import {CREATE_ARTIST_SUCCESS, FETCH_ARTIST_SUCCESS, FETCH_ALBUM_SUCCESS, FETCH_TRACK_SUCCESS, FETCH_ARTIST_ID_SUCCESS} from "./actionTypes";

export const fetchArtistSuccess = artists => {
	return {type: FETCH_ARTIST_SUCCESS, artists: artists};
};
export const fetchArtistIDSuccess = artist => {
	return {type: FETCH_ARTIST_ID_SUCCESS, artist: artist};
};
export const fetchAlbumSuccess = album => {
	console.log(album);
	return {type: FETCH_ALBUM_SUCCESS, album: album};
};
export const fetchTrackSuccess = track => {
	return {type: FETCH_TRACK_SUCCESS, tracks: track};
};

export const fetchArtists = () => {
	return dispatch => {
		axios.get('/artists').then(
			response => dispatch(fetchArtistSuccess(response.data))
		);
	}
};

export const fetchTracks = (id) => {
	return dispatch => {
		axios.get(id).then(
			response => dispatch(fetchTrackSuccess(response.data))
		);
	}
};

export const fetchArtist = (id) => {
	return dispatch => {
		axios.get(id).then(
			response => dispatch(fetchArtistIDSuccess(response.data))
		);
	}
};
export const fetchAlbum = (id) => {
	return dispatch => {
		axios.get(`/albums/${id}`).then(
			response => dispatch(fetchAlbumSuccess(response.data))
		);
	}
};

export const createArtistSuccess = () => {
	return {type: CREATE_ARTIST_SUCCESS};
};

export const createArtist = artistData => {
	return dispatch => {
		return axios.post('/artists', artistData).then(
			response => dispatch(createArtistSuccess())
		);
	};
};
export const createAlbum = artistData => {
	return dispatch => {
		return axios.post('/albums', artistData).then(
			response => dispatch(createArtistSuccess())
		);
	};
};