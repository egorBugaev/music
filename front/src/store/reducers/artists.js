import {
	FETCH_ARTIST_SUCCESS,
	FETCH_ALBUM_SUCCESS,
	FETCH_TRACK_SUCCESS,
	FETCH_ARTIST_ID_SUCCESS
} from "../actions/actionTypes";

const initialState = {
  artists: [],
  album: null,
	tracks:[],
	artist: null
};

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case FETCH_ARTIST_SUCCESS:
	    return {...state, artists: action.artists};
    case FETCH_ALBUM_SUCCESS:
	    return {...state, album: action.album};
    case FETCH_TRACK_SUCCESS:
	    return {...state, tracks: action.tracks};
    case FETCH_ARTIST_ID_SUCCESS:
	    return {...state, artist: action.artist};
    default:
      return state;
  }
};

export default reducer;