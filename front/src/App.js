import React, {Component} from 'react';
import {Route, Switch} from "react-router-dom";

import Artists from "./containers/Artists/Artists";
import Artist from "./containers/Artist/Artist";
import Tracks from "./containers/Tracks/Tracks";
import NewAlbum from "./containers/AlbumAdd/AlbumAdd";
import NewArtist from "./containers/ArtistAdd/ArtistAdd";
import Register from "./containers/Register/Register";
import Layout from "./containers/Layout/Layout";
import Login from "./containers/Login/Login";

class App extends Component {
	render() {
		return (
			<Layout>
				<Switch>
					<Route path="/" exact component={Artists}/>
					<Route path="/artists" component={Artist}/>
					<Route path="/tracks" component={Tracks}/>
					<Route path="/artist/new" exact component={NewArtist}/>
					<Route path="/album/new"  component={NewAlbum}/>
					<Route path="/register" exact component={Register}/>
					<Route path="/login" exact component={Login}/>
				</Switch>
			</Layout>
		);
	}
}

export default App;
