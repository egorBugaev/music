import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import ArtistForm from "../../components/ArtistForm/ArtistForm";
import {createArtist} from "../../store/actions/artists";

class ArtistAdd extends Component {
	createArtist = artistData => {
		this.props.onAlbumCreated(artistData).then(() => {
			this.props.history.push('/');
		});
	};

	render() {

		return (
			<Fragment>
				<PageHeader>New Artist</PageHeader>
				<ArtistForm onSubmit={this.createArtist}/>
			</Fragment>
		);
	}
}

const mapDispatchToProps = dispatch => {
	return {
		onAlbumCreated: artistData => {
			return dispatch(createArtist(artistData))
		}
	}
};

export default connect(null, mapDispatchToProps)(ArtistAdd);