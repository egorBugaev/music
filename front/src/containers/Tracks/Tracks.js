import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, PageHeader} from "react-bootstrap";
import {fetchTracks} from "../../store/actions/artists";
import {Link} from "react-router-dom";

import AlbumListItem from '../../components/AlbumListItem/AlbumListItem';

class Tracks extends Component {
  componentDidMount() {
	  this.props.onFetchTracks(this.props.history.location.pathname);
  }

  render() {
    let title =  null;
    if (this.props.album[0]){
	    title = this.props.album[0].title;
    }
    return (
      <Fragment>
        <PageHeader>
          Tracks in {title}
          <Link to="/products/new">
            <Button bsStyle="primary" className="pull-right">
              Add Album
            </Button>
          </Link>
        </PageHeader>

        {this.props.tracks.map(album => (
          <AlbumListItem
            key={album._id}
            id={album._id}
            title={album.title}
            image={album.image}
           />
        ))}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
	return {
    tracks: state.artists.tracks,
    album: state.artists.album
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchTracks: (id) => dispatch(fetchTracks(id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Tracks);