import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import AlbumForm from "../../components/AlbumForm/AlbumForm";
import {createAlbum, fetchArtist} from "../../store/actions/artists";

class AlbumAdd extends Component {

	createAlbum = albumData => {
		this.props.onAlbumCreated(albumData).then(() => {
			this.props.history.push('/');
		});
	};

	render() {
		let name =  null;
		let id = null;
		if (this.props.artist){
			id= this.props.artist[0]._id;
			name = this.props.artist[0].name;
		}

		return (
			<Fragment>
				<PageHeader>New album of {name} </PageHeader>
				<AlbumForm onSubmit={this.createAlbum} artistId={id}/>
			</Fragment>
		);
	}
}

const mapStateToProps = state => {
	return {
		artist: state.artists.artist
	}
};
const mapDispatchToProps = dispatch => {
	return {
		onAlbumCreated: albumData => {
			return dispatch(createAlbum(albumData))
		},
		onFetchArtist: (id) => dispatch(fetchArtist(id)),
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(AlbumAdd);