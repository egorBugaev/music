import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, PageHeader} from "react-bootstrap";
import {fetchArtists} from "../../store/actions/artists";
import {Link} from "react-router-dom";

import ArtistListItem from '../../components/ArtistListItem/ArtistListItem';

class Artists extends Component {
  componentDidMount() {
    this.props.onFetchArtists();
  }

  render() {
    return (
      <Fragment>
        <PageHeader>
          Artists
          <Link to="/artist/new">
            <Button bsStyle="primary" className="pull-right">
              Add Artist
            </Button>
          </Link>
        </PageHeader>

        {this.props.artists.map(artist => (
          <ArtistListItem
            key={artist._id}
            id={artist._id}
            name={artist.name}
            image={artist.image}
          />
        ))}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
	return {
    artists: state.artists.artists
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchArtists: () => dispatch(fetchArtists())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Artists);