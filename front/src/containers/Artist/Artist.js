import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, PageHeader} from "react-bootstrap";
import {fetchAlbum, fetchArtist} from "../../store/actions/artists";
import {Link} from "react-router-dom";

import AlbumListItem from '../../components/AlbumListItem/AlbumListItem';

class Artist extends Component {
	 componentDidMount() {
	 	this.props.onFetchArtist(this.props.location.pathname);
  }

  render() {
    let name =  null;
    let id = null;
    if (this.props.artist){
	    id= this.props.artist[0]._id;
	    name = this.props.artist[0].name;
	    if(!this.props.album) this.props.onFetchAlbum(this.props.artist[0]._id);
    }
    let list= null;
    if(this.props.album){
    	list=this.props.album.map(album => (
		    <AlbumListItem
			    key={album._id}
			    id={album._id}
			    title={album.title}
			    image={album.image}
		    />
	    ))
    }

	  return (
      <Fragment>
        <PageHeader>
          Albums of {name}
          <Link to={'/album/new/'+{id}}>
            <Button bsStyle="primary" className="pull-right">
              Add Album {name}
            </Button>
          </Link>
        </PageHeader>
        {list}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
	return {
    album: state.artists.album,
		artist: state.artists.artist
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchArtist: (id) => dispatch(fetchArtist(id)),
	  onFetchAlbum: (id) => dispatch(fetchAlbum(id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Artist);