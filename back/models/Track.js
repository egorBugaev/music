const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
  title: {
    type: String, required: true
  },
  description: String,
  artist:{
    type: Schema.Types.ObjectId,
    ref: 'Artist',
    required: true
  },
  album:{
	  type: Schema.Types.ObjectId,
	  ref: 'Album',
  },
	length:{
  	type: Number,
		required: true
	}
});

const Track = mongoose.model('Track', ProductSchema);

module.exports = Track;