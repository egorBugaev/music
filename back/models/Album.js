const mongoose = require('mongoose');
const Schema =  mongoose.Schema;

const AlbumSchema = new Schema({
	title: {
		type: String,
		required: true,
		unique: true
	},
	artist: {
		type: Schema.Types.ObjectId,
		ref: 'Artist',
		required: true
	},
	tracks:{
		type: Array,
		required: true
	},
	image: String,
	description: String
});

const Album = mongoose.model('Album', AlbumSchema);

module.exports = Album;