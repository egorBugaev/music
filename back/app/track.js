const express = require('express');
const Track = require('../models/Track');
const Album = require('../models/Album');

const createRouter = () => {
	const router = express.Router();

	router.get('/', (req, res) => {
		Track.find()
			.then(results => res.send(results))
			.catch(() => res.sendStatus(500));
	});

	router.post('/', (req, res) => {
		const track = new Track(req.body);

		track.save()
			.then(result => res.send(result))
			.catch(error => res.status(400).send(error));
	});

	router.get('/:id', (req, res) => {
		Album.find().populate('track')
			.find({_id: req.params.id})
			.then(result => {
				if (result){
					Track.find(result.tracks)
						.then(result => {
							if (result)	res.send(result);
							else res.sendStatus(404);
						} );
				}
				else res.sendStatus(404);
			})
			.catch(() => res.sendStatus(500));
	});

	return router;
};

module.exports = createRouter;
