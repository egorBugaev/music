const express = require('express');
const multer = require('multer');
const Artist = require('../models/Artist');
const Album = require('../models/Album');

const upload = multer({ storage: multer.memoryStorage({}) });

const router = express.Router();

const createRouter = (db) => {
    // Artist index
    router.get('/', (req, res) => {
        Artist.find()
            .then(results =>res.send(results))
            .catch(() => res.sendStatus(500));
    });
    // Artist create
    router.post('/', upload.single('image'), (req, res) => {
        const artistData = req.body;
	    if (req.file) {
		    artistData.image = new Buffer(req.file.buffer).toString("base64");
        } else {
            artistData.image = null;
        }

        const artist = new Artist(artistData);

        artist.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });
	router.get('/:id', (req, res) => {
		Artist.find()
			.find({_id: req.params.id})
			.then(result => {
				if (result) res.send(result);
				else res.sendStatus(404);
			})
			.catch(() => res.sendStatus(500));
	});

    return router;
};

module.exports = createRouter;